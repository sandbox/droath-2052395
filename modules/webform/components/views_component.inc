<?php

/**
 * @file
 * The webform component implementations for the views component module.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_views_component() {
  return array(
    'name' => NULL,
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => NULL,
    'extra' => array(
      'title_display' => 'above',
      'views_component' => array(
        'view' => NULL,
        'filter' => array(
          'enable' => FALSE,
          'select_label' => NULL,
          'empty_option' => t('- Select -'),
        ),
        'more_options' => NULL,
      ),
      'private' => FALSE,
    ),
  );
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_views_component() {
  return array(
    'webform_display_views_component' => array(
      'render element' => 'element',
      'path' => drupal_get_path('module', 'views_component'),
      'file' => '/modules/webform/components/views_component.inc',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_views_component($component) {
  $form     = array();
  $settings = $component['extra']['views_component'];

  $options = views_component_get_views_component_options();
  if (empty($options)) {
    drupal_set_message(t("You don't have any views component(s) defined."), 'error');
  }
  $form['extra']['views_component'] = array(
    '#type' => 'fieldset',
    '#title' => t('Views component'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['extra']['views_component']['view'] = array(
    '#type' => 'select',
    '#title' => t('Views Component'),
    '#description' => t('Select a views component.'),
    '#options' => $options,
    '#empty_option' => t('- Select -'),
    '#required' => TRUE,
    '#default_value' => $settings['view'],
  );
  $form['extra']['views_component']['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter'),
    '#description' => t('The filter functionality is contingent on the selected
     views component options having a data-filter attribute set.'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );
  $form['extra']['views_component']['filter']['enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Filter'),
    '#description' => t('Filter the views component options'),
    '#default_value' => $settings['filter']['enable'],
  );
  $form['extra']['views_component']['filter']['select_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Select Label'),
    '#default_value' => $settings['filter']['select_label'],
    '#states' => array(
      'visible' => array(
        ':input[name="extra[views_component][filter][enable]"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['extra']['views_component']['filter']['empty_option'] = array(
    '#type' => 'textfield',
    '#title' => t('Empty Option'),
    '#default_value' => $settings['filter']['empty_option'],
    '#states' => array(
      'visible' => array(
        ':input[name="extra[views_component][filter][enable]"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['extra']['views_component']['more_options'] = array(
    '#type' => 'textarea',
    '#title' => t('More Options'),
    '#description' => t('More options are appended to the views component
      options. <br/> <strong>NOTE:</strong> Only input one option per
      line using the following format "value|text".'),
    '#rows' => 5,
    '#default_value' => $settings['more_options'],
    '#element_validate' => array('_views_component_more_options_validate'),
  );

  return $form;
}

/**
 * Element validation callback to validate the more options values.
 */
function _views_component_more_options_validate($element, &$form_state) {
  $value = $element['#value'];

  // Only validate if we have an existing value to check.
  if (isset($value) && !empty($value)) {

    // Iterate through the more options and make sure they are valid.
    foreach (explode("\r\n", $value) as $option) {
      if (!preg_match('/^(.+(?=\|))\|(.+)$/', $option)) {
        form_error($element, t('@element_title are incorrectly formated.',
          array('@element_title' => $element['#title']))
        );
        return;
      }
    }
  }
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_views_component($component, $value = NULL, $filter = TRUE) {
  $value    = is_array($value) ? reset($value) : $value;
  $settings = $component['extra']['views_component'];
  if (!isset($settings['view']) || empty($settings['view'])) {
    return array();
  }
  list($view_name, $view_display) = explode(':', $settings['view']);

  $element = array(
    '#type' => 'views_component',
    '#view_name' => $view_name,
    '#view_display' => $view_display,
    '#title' => $filter ? _webform_filter_xss($component['name']) : $component['name'],
    '#required' => $component['mandatory'],
    '#weight' => $component['weight'],
    '#default_value' => $filter ? _webform_filter_values($component['value']) : $component['value'],
    '#theme_wrappers' => array('webform_element'),
    '#prefix' => '<div class="webform-views-component" id="webform-views-component' . $component['form_key'] . '">',
    '#suffix' => '</div>',
  );

  // Merge more options with the views component options.
  if (isset($settings['more_options']) && !empty($settings['more_options'])) {
    $element['#more_options'] = views_component_build_more_options($settings['more_options']);;
  }

  // Filter views component options.
  if (isset($settings['filter']) && $settings['filter']['enable']) {
    $element['#filter'] = array(
      'select_label' => $settings['filter']['select_label'],
      'empty_option' => $settings['filter']['empty_option'],
      'wrapper' => 'webform-views-component' . $component['form_key'],
    );
  }

  if (!empty($value)) {
    $element['#default_value'] = $value;
  }

  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_views_component($component, $value, $format = 'html') {
  $value = reset($value);
  $element = array(
    '#title' => $component['name'],
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_views_component',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#format' => $format,
    '#value' => isset($value) ? $value : NULL,
    '#options' => _views_component_build_options_by_component($component),
  );

  return $element;
}

/**
 * Theme webform display views component.
 */
function theme_webform_display_views_component($variables) {
  $element = $variables['element'];

  // Display the option text for the submitted value.
  $option_value = isset($element['#options'][$element['#value']]) ? $element['#options'][$element['#value']] : NULL;
  return $element['#format'] == 'html' ? check_plain($option_value) : $option_value;
}

/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_views_component($component, $sids = array(), $single = FALSE) {
  $options = _views_component_build_options_by_component($component);

  // Generate a lookup table of results.
  $query = db_select('webform_submitted_data', 'wsd')
    ->fields('wsd', array('data'))
    ->condition('nid', $component['nid'])
    ->condition('cid', $component['cid'])
    ->condition('data', '', '<>')
    ->groupBy('data');
  $query->addExpression('COUNT(sid)', 'count');

  if (count($sids)) {
    $query->condition('sid', $sids, 'IN');
  }
  $result = $query->execute();

  $rows = array();
  foreach ($result->fetchAll(PDO::FETCH_ASSOC) as $index => $record) {
    $data = isset($options[$record['data']]) ? check_plain($options[$record['data']]) : NULL;
    if (empty($data)) {
      continue;
    }

    $rows[$index] = array($data, $record['count']);
  }

  return $rows;
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_views_component($component, $value) {
  $options = _views_component_build_options_by_component($component);

  $value = is_array($value) ? reset($value) : NULL;
  return isset($options[$value]) ? check_plain($options[$value]) : '';
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_views_component($component, $export_options) {
  $header = array();
  $header[0] = NULL;
  $header[1] = NULL;
  $header[2] = $component['name'];

  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_views_component($component, $export_options, $value) {
  $value   = is_array($value) ? reset($value) : NULL;
  $options = _views_component_build_options_by_component($component);

  if (!isset($options[$value])) {
    return NULL;
  }

  return $options[$value];
}

/**
 * Build a one-dimensional options array based on the defined views component.
 */
function _views_component_build_options_by_component($component) {
  if (!isset($component) || empty($component)) {
    return array();
  }
  $views_component = $component['extra']['views_component'];

  $view    = $views_component['view'];
  $options = &drupal_static(__FUNCTION__ . ':' . $view, array());

  if (empty($options)) {
    list($view_name, $display) = explode(':', $views_component['view']);
    $options = views_component_build_options_by_view_name($view_name, $display);

    if (!empty($views_component['more_options'])) {
      $options = array_merge($options, views_component_build_more_options(
        $views_component['more_options']));
    }
    $options = views_component_flatten_options_array($options);
  }

  return $options;
}
