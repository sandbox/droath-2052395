<?php

/**
 * @file
 * Define the views plugins for the views component module.
 */

/**
 * Implements hook_views_plugins().
 */
function views_component_views_plugins() {
  return array(
    'display' => array(
      'views_component' => array(
        'title' => t('Views component'),
        'admin' => t('Views component'),
        'help' => t('Display a view as a form element.'),
        'handler' => 'views_component_plugin_display_component',
        'use ajax' => FALSE,
        'use pager' => FALSE,
        'use more' => FALSE,
        'accept attachments' => FALSE,
        'render views component' => TRUE,
      ),
    ),
    'style' => array(
      'views_component_options' => array(
        'title' => t('Views component options'),
        'admin' => t('Views component options'),
        'help' => t('Build the options output for the views component.'),
        'handler' => 'views_component_plugin_style_component_options',
        'uses options' => TRUE,
        'uses row plugin' => TRUE,
        'uses row class' => FALSE,
        'uses fields' => TRUE,
        'uses grouping' => FALSE,
        'even empty' => FALSE,
        'type' => 'normal',
      ),
    ),
  );
}
