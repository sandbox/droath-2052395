; (function ( $ ) {

  $.fn.selectBoxFilter = function ( options ) {

    var settings = $.extend({
      selectLabel : null,
      emptyOption : '- Select -',
      dataAttribute : 'filter'
    }, options );

    var optSelected = {};
    var optDefined  = [];
    var optFileterd = [];

    var selectBoxParent = this;
    var selectBoxParentId = selectBoxParent.attr('id');

    var selectBox = selectBoxParent.find('select');
    var selectBoxFilterId = selectBoxParentId;

    selectBoxParent.hide();
    selectBoxParent.before('<div id="' + selectBoxFilterId + '-filter"><select id="' + selectBoxFilterId + '-filter-select"><option value="">' + settings.emptyOption + '</select></div>');

    if (settings.selectLabel) {
      //  Clone the select box parent label and display the new label using the
      //  one defined in the options.
      var selectBoxLabelChildren = selectBox.siblings('label').children().clone();
      $('#' + selectBoxFilterId + '-filter-select').before(selectBox.siblings('label').clone().text(settings.selectLabel).append(selectBoxLabelChildren));
    }

    selectBox.children().each( function (index, value) {
      var option = $(value);
      var optObj = option.is('optgroup') ? option.children() : option;

      $.each( optObj, function (optIndex, optValue) {
        var obj = {}
        var optFilter = $(optValue).data(settings.dataAttribute);

        // Build the filter option based on the unique data attribute.
        if ( $.inArray(optFilter, optDefined) == -1 && optFilter !== undefined ) {
          $('#' + selectBoxFilterId + '-filter-select').append('<option value="' + optFilter + '">' + optFilter + '</option>');
          optDefined.push(optFilter);
        }

        obj['option'] = optValue;
        obj['filter'] = optFilter;
        obj['selected'] = null;

        // Set the filter option selected attribute based on if the filtered
        // option has been selected.
        if ( $(optValue).attr('selected') ) {
          optSelected = optValue;
          obj['selected'] = $(optSelected).val();
          $('#' + selectBoxFilterId + '-filter-select option[value="' + optFilter + '"]').attr('selected', 'selected');
        }

        optFileterd.push(obj);
      });
    });

    // We'll need to initialize the select box manually if an option is already
    // selected.
    if ( !$.isEmptyObject(optSelected) ) {
      var selectedFilter = $(optSelected).data(settings.dataAttribute);
      initSelectBoxFilter(selectedFilter);
    }

    // We'll need to sit here patiently and listen for a change event.
    $('#' + selectBoxFilterId + '-filter-select').change( function() {
      var selectedFilter = $(this).val();
      initSelectBoxFilter(selectedFilter);
    });

    // Initialize the select box filter.
    function initSelectBoxFilter(selectedFilter) {
      if (selectedFilter) {
        var optSelectedVal = null,
            optFilterString = '',
            optParentArr = [];

        // Build the options and append them to the select box.

        $.each( optFileterd, function (index, value) {
          if ( value.filter == selectedFilter || value.filter == undefined ) {
            var option     = value.option,
                optParent  = $(option).parent(),
                optLabel   = optParent.attr('label');

            if(optParent.is('optgroup')){
              if($.inArray(optLabel, optParentArr) == -1){
                optFilterString += optParent[0].outerHTML;
              }

              optParentArr.push(optLabel);

            } else {
              optFilterString += option.outerHTML;
            }

            if (value.selected != null) {
              optSelectedVal = value.selected;
            }

          }

        });

        selectBox.html(optFilterString);

        // Set the select box to the selected option value or an empty string.
        selectBox.val( (optSelectedVal != null ? optSelectedVal : '') );

        selectBoxParent.show();
      } else {
        selectBoxParent.hide();
      }
    }
  }
}( jQuery ));
