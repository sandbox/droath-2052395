; ( function ( $ ) {
  Drupal.behaviors.views_component = {
    attach: function (context, settings) {
      var component = settings.views_component;
      $('#' + component.wrapper).selectBoxFilter({
        selectLabel: component.select_label,
        emptyOption: component.empty_option
      });
    }
  }
}( jQuery ));
