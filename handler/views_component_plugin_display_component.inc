<?php

/**
 * @file
 * The views display plugin for the views component module.
 */

class views_component_plugin_display_component extends views_plugin_display {

  /**
   * Define the available component elements.
   */
  public function get_component_elements() {
    return array(
      'div' => t('Div'),
      'select' => t('Select'),
    );
  }

  /**
   * Define the option definition.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['views_component_name']['default'] = NULL;
    $options['views_component_settings']['default'] = array(
      'label' => NULL,
      'element' => 'div',
    );

    return $options;
  }

  /**
   * The options summary overview.
   */
  public function options_summary(&$categories, &$options) {
    parent::options_summary($categories, $options);
    $categories['views_component'] = array(
      'title' => t('Views component'),
      'column' => 'second',
      'build' => array(
        '#weight' => -10,
      ),
    );
    $component_name = $this->get_option('views_component_name');
    $options['views_component_name'] = array(
      'category' => 'views_component',
      'title' => t('Name'),
      'value' => empty($component_name) ? t('None') : $component_name,
    );
    $options['views_component_settings'] = array(
      'category' => 'views_component',
      'title' => t('Settings'),
      'value' => t('View settings'),
    );
  }

  /**
   * The options settings form.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'views_component_name':
        $form['views_component_name'] = array(
          '#type' => 'textfield',
          '#title' => t('Name'),
          '#description' => t('The name of the views component.'),
          '#default_value' => $this->get_option('views_component_name'),
        );
        break;

      case 'views_component_settings':
        $settings = $this->get_option('views_component_settings');
        $form['label'] = array(
          '#type' => 'textfield',
          '#title' => t('Default label'),
          '#description' => t('The default label for the view component.'),
          '#default_value' => $settings['label'],
        );
        $form['element'] = array(
          '#type' => 'select',
          '#title' => t('Element'),
          '#options' => $this->get_component_elements(),
          '#description' => t('The HTML element to use for the view component.'),
          '#default_value' => $settings['element'],
        );
        break;
    }
  }

  /**
   * The options submit handler.
   */
  public function options_submit(&$form, &$form_state) {
    parent::options_submit($form, $form_state);
    $values   = $form_state['values'];
    $selector = $form_state['section'];

    switch ($selector) {
      case 'views_component_name':
        $this->set_option('views_component_name', $values['views_component_name']);
        break;

      case 'views_component_settings':
        $this->set_option('views_component_settings', array_intersect_key($values, $this->options[$selector]));
        break;
    }

    views_component_clear_cache();
  }

  /**
   * Render the views component theme.
   */
  public function render() {
    $settings = $this->get_option('views_component_settings');

    $theme_component = array(
      '#view' => $this->view,
      '#tag' => $settings['element'],
    );
    if (!empty($settings['label'])) {
      $theme_component['#title'] = $settings['label'];
    }

    return $theme_component;
  }

  /**
   * Validate the views component display.
   */
  public function validate() {
    $errors = parent::validate();

    // Validate the style plugin as the views component display is only
    // designed to work with the views component options format style.
    $style = $this->get_plugin();
    if ($style->plugin_name != 'views_component_options') {
      $errors[] = t('Display "@display" needs to use a views component
        options for the format style.', array('@display' => $this->display->display_title));
    }

    return $errors;
  }

  /**
   * Render the views component preview.
   */
  public function preview() {
    $render = $this->view->render();

    if ($this->view->preview) {
      $render['#type'] = 'views_component';
      // Views doesn't seem to call the element #process callback. So will
      // just populate the options element ourself. This seems a little hackish
      // but it's only used when rendering in views preview.
      $render['options'] = $this->get_plugin()->theme_component_options();
      return drupal_render($render);
    }

    return $render;
  }
}
