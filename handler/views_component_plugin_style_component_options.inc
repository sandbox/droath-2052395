<?php

/**
 * @file
 * The views style plugin for the views component module.
 */

class views_component_plugin_style_component_options extends views_plugin_style {

  /**
   * Define the option definition.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['component']['default'] = array(
      'type' => NULL,
      'text' => NULL,
      'attributes' => array(
        'value' => NULL,
        'data_filter' => NULL,
      ),
      'option_group' => NULL,
      'empty_option' => NULL,
    );

    return $options;
  }

  /**
   * The options settings form.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['component'] = array(
      '#type' => 'fieldset',
      '#title' => t('Component configurations'),
      '#description' => t('<strong>NOTE:</strong> The render output for fields
        that are configured below will be stripped of HTML markup.'),
    );
    $options = $this->options['component'];

    // Retrieve the available field labels.
    $field_options = $this->display->handler->get_field_labels();

    $form['component']['text'] = array(
      '#type' => 'select',
      '#title' => t('Text'),
      '#description' => t('Select the field to render as the component text.'),
      '#empty_option' => t('- Select -'),
      '#options' => $field_options,
      '#default_value' => $options['text'],
      '#required' => TRUE,
    );

    // Determine what type of component we are configuring.
    $form['component']['type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#empty_option' => t('- Select -'),
      '#options' => $this->get_component_type(),
      '#default_value' => $options['type'],
      '#required' => TRUE,
    );

    // Component option configurations.
    $form['component']['option_group'] = array(
      '#type' => 'select',
      '#title' => t('Option group'),
      '#description' => t('Select the field to use as the component option
        group.'),
      '#empty_option' => t('- None -'),
      '#options' => $field_options,
      '#default_value' => $options['option_group'],
      '#dependency' => array(
        'edit-style-options-component-type' => array('option'),
      ),
    );
    $form['component']['empty_option'] = array(
      '#type' => 'textfield',
      '#title' => t('Empty option'),
      '#description' => t('Append an empty option to the options
        (e.g., Select).'),
      '#default_value' => $options['empty_option'],
      '#dependency' => array(
        'edit-style-options-component-type' => array('option'),
      ),
    );
    // Component attributes configurations.
    $form['component']['attributes'] = array(
      '#type' => 'fieldset',
      '#title' => t('Attributes'),
    );
    $form['component']['attributes']['value'] = array(
      '#type' => 'select',
      '#title' => t('Value'),
      '#description' => t('Select the field to use for the value attribute.'),
      '#options' => $field_options,
      '#empty_option' => t('- Select -'),
      '#default_value' => $options['attributes']['value'],
      '#required' => TRUE,
    );
    $form['component']['attributes']['data_filter'] = array(
      '#type' => 'select',
      '#title' => t('Data filter'),
      '#description' => t('Select the field to use for the meta-data attribute.'),
      '#options' => $field_options,
      '#empty_option' => t('- None -'),
      '#default_value' => $options['attributes']['data_filter'],
    );
  }

  /**
   * Define the component type.
   */
  public function get_component_type() {
    return array(
      'option' => t('Option'),
    );
  }

  /**
   * Retrieve the component option by name.
   */
  public function get_component_option($name) {
    $component = $this->options['component'];
    if (!isset($component[$name]) || empty($component[$name])) {
      return NULL;
    }

    return $component[$name];
  }

  /**
   * Strip HTML tags from text.
   */
  public function strip_tags($text) {
    return trim(strip_tags(htmlspecialchars_decode($text)));
  }

  /**
   * Render the component options.
   */
  public function render_component_options($records, $config, $selected) {
    $type         = $config['type'];
    $text_field   = $config['text'];
    $group_field  = $config['option_group'];
    $attributes   = $config['attributes'];

    $options = array();
    $has_optgroup = ($type == 'option' && isset($group_field)) ? TRUE : FALSE;

    $this->render_fields($this->view->result);

    // Iterate through the views results.
    foreach ($records as $index => $record) {
      if (!isset($this->view->field[$text_field]) ||
        !isset($this->view->field[$attributes['value']])) {
        continue;
      }
      $text  = $this->strip_tags($this->get_field($index, $text_field));
      $value = $this->strip_tags($this->get_field($index, $attributes['value']));

      // Bail out if we don't have text or a value defined as this is required
      // for all views component options.
      if (empty($text) || empty($value)) {
        continue;
      }

      // Continue if a option group has been defined.
      if ($has_optgroup) {
        $group_name = NULL;

        // Insure the field has content available.
        if (isset($this->view->field[$group_field])) {
          $group_name = $this->get_field($index, $group_field);
        }
      }
      $parameters = array(
        'text' => $text,
        'attributes' => array(
          'value' => $value,
        ),
      );

      // Add the selected attribute if value equals the selected value.
      if ($selected == $value) {
        $parameters['attributes']['selected'] = 'selected';
      }

      // Add the data-filter attribute if content exist.
      if (isset($this->view->field[$attributes['data_filter']])) {
        $parameters['attributes']['data-filter'] = $this->get_field($index, $attributes['data_filter']);
      }

      // Set the component parameters.
      if (isset($group_name)) {
        $options[$group_name][] = $parameters;
      }
      else {
        $options[] = $parameters;
      }
    }

    return $options;
  }

  /**
   * Validate the views component options style plugin.
   */
  public function validate() {
    $errors = parent::validate();

    $display = $this->display;
    if ($display->display_plugin != 'views_component' &&
      !$display->handler->is_default_display()) {

      // Set an error if the display is not using a views component plugin.
      $errors[] = t('Style "@style" requires a views component display.',
        array('@style' => $this->definition['title']));
    }

    return $errors;
  }

  /**
   * Retrieve the component options as an array.
   */
  public function get_component_options($value = NULL) {
    return $this->render_component_options($this->view->result,
      $this->options['component'], $value);
  }

  /**
   * Build the component theme.
   */
  public function theme_component_options($value = NULL) {
    $theme_options = array(
      '#tag' => $this->get_component_option('type'),
      '#theme' => 'views_component_options',
      '#options' => $this->get_component_options($value),
    );

    if ($empty_option = $this->get_component_option('empty_option')) {
      $theme_options['#empty_option'] = t('@empty_option',
        array('@empty_option' => $empty_option));
    }

    return $theme_options;
  }

  /**
   * Render the style plugin.
   */
  public function render() {
    if ($this->uses_row_plugin() && empty($this->row_plugin)) {
      debug('views_plugin_style_default: Missing row plugin');
      return;
    }
    $theme = $this->theme_component_options();

    return drupal_render($theme);
  }
}
